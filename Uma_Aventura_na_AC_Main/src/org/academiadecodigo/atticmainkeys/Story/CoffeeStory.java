package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class CoffeeStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;

    public CoffeeStory(InputStream in, PrintStream out) {
        prompt = new Prompt(this.in=in,this.out=out);
    }

    public void goToBeginning() {
        History history = new History(in,out);
        history.start();
    }

    public void goTo4() {

        out.println(Message._4);
        String[] options = {Message._9, Message._10};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo66();
                break;
            case 2:
                goTo58();
                break;
            default:
                break;
        }
    }

    public void goTo66() {
        out.println(Message._66);
        UlissesStory ulissesStory = new UlissesStory(in,out);
        ulissesStory.goTo3();
    }

    public void goTo58() {

        out.println(Message._58);
        String[] options = {Message._59, Message._60};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo61();
                break;
            case 2:
                System.out.println(Message._67);
                break;
            default:
                break;
        }
    }

    public void goTo61() {

        out.println(Message._61);
        String[] options = {Message._62, Message._63};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo43();
                break;
            case 2:
                goTo47();
                break;
            default:
                break;
        }
        //TODO alterar booleano que depois vai alterar invocação do metodo
    }

    public void goTo43() {
        out.println(Message._43);
        String[] options = {Message._44, Message._45};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
            case 2:
                goTo46();
                break;
            default:
                break;
        }
    }

    public void goTo46() {
        out.println(Message._46);
        //TODO give AM and dzrt CD
        goTo47();
    }

    public void goTo47() {
        out.println(Message._47);
        String[] options = {Message._48, Message._49};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo51();
                break;
            case 2:
                goTo50();
                break;
            default:
                break;
        }
    }

    public void goTo50() {
        out.println(Message._50);
        //TODO fazer mensagem de ligação com o luis
        MCRoomStory mcRoomStory = new MCRoomStory(in,out);
        out.println(Message._34);
        mcRoomStory.goTo34();

    }

    public void goTo51() {
        out.println(Message._51);
        String[] options = {Message._52, Message._53};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo68or69();
                break;
            case 2:
                goTo53();
                break;
            default:
                break;
        }
    }

    public void goTo68or69() {
        //TODO condiççao print 68 ou 69
        out.println(Message._68);
    }

    public void goTo53() {

        MCRoomStory mcRoomStory = new MCRoomStory(in,out);
        out.println(Message._20);
        mcRoomStory.goTo20();
    }



    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void useThreadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
