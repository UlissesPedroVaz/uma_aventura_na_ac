package org.academiadecodigo.atticmainkeys.Story;

public class Message {

    public static final String _INTRO = "\nOh não! A Sara desapareceu! Nunca mais a vamos ver chegar com o seu casaco guna amarelo :(\n" +
            "... ou maybe vamos?\n" +
            "'VEeEMMM COMIGO VIVEeEeeR, UMA AVENTURA'\n" +
            "A tua missão é encontrar a Sara. Ao longo do jogo vais ter de encarnar o Sherlock Holmes que há dentro de ti e descobrir porque é que ela não se dignou a vir trabalhar hoje.\n";
    public static final String _0 = "\nAcabaste de chegar à Academia e vais começar a tua investigação.\n" +
            "Tens 3 opções: ir à sala dos MC's, ir beber um café ao pátio ou ir falar com o Ulisses que acabou agora de entrar e medir a temperatura.\n";
    public static final String _1 = "O que queres fazer?";
    public static final String _2 = "Ir à sala dos MC's";
    public static final String _3 = "Falar com o Ulisses";
    public static final String _4 = "Ir beber café ao pátio";
    public static final String _5 = "Perguntar ao Ruivo pela Sara";
    public static final String _6 = "Perguntar ao Rolo pela Sara";
    public static final String _7 = "Falar com o Rolo da Wish";
    public static final String _8 = "Abraçar o Ulisses";
    public static final String _9 = "Falar com a Bri";
    public static final String _10 = "Constatar o grande nariz do Zé";
    public static final String _11 = "Resposta filosófica do Ruivo --> 'Ok...'";
    public static final String _12 = "\nRolo: 'Epa, eu não a vi. Mas o Bento vive com ela, vai falar com ele.'\n" +
            "Embora o Rolo tenha sido um pouco inútil, não foi por completo.\n" +
            "Tu sobes as escadas para ficares mais perto do céu e vês o Bento a saltitar na bola de pilates.";
    public static final String _12_1 = "\nBento: 'E quie?'";
    public static final String _12_2 = "\nTu: 'Viste a Sara?'";
    public static final String _13 = "\nBento: 'Não a vejo deste ontem. Ela foi jantar ao Mercado Oriental.'\n\nTu vais ao Mercado Oriental à procura de pistas.";
    public static final String _14 = "Falar com o dono do restaurante";
    public static final String _15 = "'Ahn?'";
    public static final String _16 = "'Cumo?'";
    public static final String _17 = "\nSenhor: '나는 그것에 대해 알고 싶지 않습니다.'";
    public static final String _17_1 = "Isto não vai resultar...\n\n" +
            "Estás a precisar e vais à casa de banho.";
    public static final String _18 = "Ir à casa de banho";
    public static final String _18_1 = "\nO que é aquilo ao pé da sanita?\n" +
            "Aproximas-te e vês... dois CDs? Quem é que ainda tem CDs?\n" +
            "Espera... São CDs dos Tokyo Hotel e dos D'ZRT! Isto certamente tem de ser da Sara!!";
    public static final String _19 = "Voltar à Academia";
    public static final String _20 = "Ir a casa da Sara";
    public static final String _20_1 = "\nAcabaste de chegar a casa da Sara. Realisticamente, só tens duas opções buddy.";
    public static final String _21 = "Tocar à campainha";
    public static final String _21_1 = "\nEsperas um bocado e ouves alguém que parece muito o Nuno falar pelo intercomunicador.";
    public static final String _21_2 = "Nuno??: 'Não me chateiem! Tenho de limpar a casa de banho que a Sara sujou ontem à noite e agora não acorda.'";
    public static final String _21_3 = "Vais mesmo ter de ligar ao António, mas bates palmas ao Nuno à mesma.";
    public static final String _22 = "Ligar ao António";
    public static final String _23 = "'Preciso da chave da casa da Sara'";
    public static final String _24 = "'Preciso de uma aparelhagem'";
    public static final String _24_1 = "\nO António é brutal e traz uma aparelhagem.";
    public static final String _25 = "Tocar Tokyo Hotel";
    public static final String _26 = "Tocar Arctic Monkeys";
    public static final String _27 = "Tocar D'ZRT";
    public static final String _28 = "Sérgio ouve a música --> 'Tens um ótimo gosto musical' --> Recomenda carreira musical --> Desistir da Academia";
    public static final String _29 = "Sara sai de casa como uma cobra encantada --> A Sara foi encontrada";
    public static final String _30 = "Dizer ao Rolo da Wish que ele não sabe dançar --> 'Vai pó caralho!'";
    public static final String _31 = "Perguntar pela Sara --> 'A Sopas vive com ela'";
    public static final String _32 = "Falar com a Sopas";
    public static final String _33 = "'O que estás a fazer?' --> 'Lisboa é merda, estou a ver a Casa da Música no Porto' --> 'Lisboa é fixe!!' --> Sair porta fora";
    public static final String _34 = "Encontrar o Luís que oferece:";
    public static final String _35 = "Livros de programação";
    public static final String _36 = "CDs tuga dos anos 2000";
    public static final String _37 = "O Sérgio aparece";
    public static final String _38 = "Perguntar algo sobre exceptions --> Sérgio dá uma palestra infinita e a Sara nunca é encontrada";
    public static final String _39 = "Pedir o número de telemóvel da Sara --> 'Está no tinder, já devias saber' --> Ligar à Sara";
    public static final String _40 = "A Sara não atende";
    public static final String _41 = "A Sara atende e tocas-lhe o CD dos D'ZRT --> A Sara foi encontrada";
    public static final String _42 = "Perguntar pela Sara --> Viu a Sara no pingpong";
    public static final String _43 = "O Bruno e o Casimiro estão a jogar pingpong --> Vês a cabeça rapada da Sara";
    public static final String _44 = "Ir investigar";
    public static final String _45 = "Investigar ir";
    public static final String _46 = "Descobrir que é o Allan --> O Allan oferece um CD dos Arctic Monkeys e um dos D'ZRT";
    public static final String _47 = "A Mariana passa e convida a ir aos irmãos";
    public static final String _48 = "Ir aos irmãos";
    public static final String _49 = "Não ir aos irmãos";
    public static final String _50 = "Sofia+Prima+Yoca+Chico enganam-se no grupo do slack e escrevem nos atticmainkeys que a Sara gosta dos D'ZRT";
    public static final String _51 = "Encontrar um trilho de sushi no caminho";
    public static final String _52 = "Comer pedaço do chão";
    public static final String _53 = "Seguir trilho de sushi até à casa da Sara";
    public static final String _54 = "O Ruben passa a gritar AVIÃO";
    public static final String _55 = "Dar 5€ ao Ruben";
    public static final String _56 = "Não dar 5€ ao Ruben, ele que se foda";
    public static final String _57 = "A Luísa passa e dá o jar do farty unicorn --> Ir para casa jogar";
    public static final String _58 = "O Zé diz merda";
    public static final String _59 = "Gozar com o Zé";
    public static final String _60 = "Perguntar ao Zé viu a Sara";
    public static final String _61 = "Seguir o barbas que está a rir tanto que corre para a casa de banho";
    public static final String _62 = "Reparar no barulho da sala de pingpong";
    public static final String _63 = "Ficar com o Barbas que está a vomitar de tanto rir";
    public static final String _CHOOSE = "Queres:";

    public static final String _64 = "Ruben: 'Obrigado amigo, bora ao Avião que se foda a Sara!'\n===========YOU WIN==========\nGanhaste com Pay2Win, não sabes da Sara, mas foste ao Avião e por isso tudo ficou bem!";
    public static final String _65 = "Ruben: 'Então vai pó caralho!'";

    public static final String _66 = "'O Ulisses estava à tua procura'";
    public static final String _67 = "Zé: 'Gozaste com o meu nariz, por isso GET FUCKED!!'\n\n\n===========YOU LOSE==========\n\nO Zé mandou-te um clássico GET FUCKED dos seus. Não aguentaste e foste para casa chorar...";
    public static final String _68 = "\n\n\n===========YOU LOSE==========\n\nNo que é que estavas a pensar?! Foste juntar-te ao Barbas na casa de banho e passaste lá o resto do dia...";
    public static final String _69 = "\n\n\n===========YOU LOSE==========\n\nNo que é que estavas a pensar?! Passaste o resto do dia na casa de banho...";
    public static final String _70 = "António: 'Metes-me nojo! Volta para a Academia, tens aulas!'\n\n\n===========YOU LOSE==========\n\nINVASOR DE PROPRIEDADE PRIVADA! O António ficou repugnado com a tua falta de civismo...";
    public static final String _71 = "\nSenhor: '난 그녀에게 지쳤어, 그녀는 항상 여기 있어. 그리고 그녀는 그녀가 한국말을 아주 잘한다고 생각하지만 그것은 거짓말입니다.'";

}
