package org.academiadecodigo.atticmainkeys.Story;

import com.sun.corba.se.spi.activation.Server;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.awt.*;
import java.io.InputStream;
import java.io.PrintStream;

public class MCRoomStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;

    public MCRoomStory(InputStream in, PrintStream out) {
        prompt = new Prompt(this.in=in,this.out=out);
    }

    public void goToBeginning() {
        History history = new History(in,out);
        history.start();
    }

    public void goTo2() {

        threadSleep(700);

        out.println("\nEstás à porta da sala dos MC's. Vês o Ruivo, o Rolo e o Rolo da Wish.");

        String[] options = {Message._5, Message._6, Message._7};
        int choice = useMenuScanner(options, Message._CHOOSE);

        threadSleep(500);

        switch (choice) {
            case 1:
                goTo5();
                break;
            case 2:
                goTo6();
                break;
            case 3:
                goTo7();
                break;
            default:
                break;
        }

    }

    public void goTo5() {

        out.println(Message._11);
        out.println(Message._2);
        goTo2();

    }

    public void goTo6() {
        out.println(Message._12);
        threadSleep(500);
        out.println(Message._12_1);
        threadSleep(500);
        out.println(Message._12_2);
        threadSleep(500);
        out.println(Message._13);

        threadSleep(500);

        String[] options = {Message._14, Message._18};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo14();
                break;
            case 2:
                goTo18();
                break;
            default:
                break;
        }

    }

    public void goTo7() {

        String[] options = {Message._30, Message._31};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo30();
                break;
            case 2:
                goTo31();
                break;
            default:
                break;
        }

    }

    public void goTo14() {

        threadSleep(500);
        out.println(Message._71);
        threadSleep(500);

        String[] options = {Message._15, Message._16};
        int choice = useMenuScanner(options, "WTF?");

        switch (choice) {
            case 1:
                goTo15();
                break;
            case 2:
                goTo16();
                break;
            default:
                break;
        }

    }

    public void goTo15() {

        threadSleep(500);
        out.println(Message._17);
        goTo17();

    }

    public void goTo16() {

        out.println(Message._17);
        goTo17();

    }

    public void goTo17() {

        threadSleep(500);
        out.println(Message._17_1);
        threadSleep(500);
        goTo18();

    }

    public void goTo18() {

        threadSleep(500);
        out.println(Message._18_1);
        threadSleep(700);

        String[] options = {Message._19, Message._20};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo19();
                break;
            case 2:
                goTo20();
                break;
            default:
                break;
        }

    }

    public void goTo19() {
        out.println(Message._INTRO);
        goToBeginning();
    }

    public void goTo20() {

        threadSleep(500);
        out.println(Message._20_1);
        threadSleep(500);

        String[] options = {Message._21, Message._22};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo21();
                break;
            case 2:
                goTo22();
                break;
            default:
                break;
        }

    }

    public void goTo21() {

        threadSleep(500);
        out.println(Message._21_1);
        threadSleep(500);
        out.println(Message._21_2);
        threadSleep(500);

        out.println(Message._21_3);
        goTo22();

    }

    public void goTo22() {

        String[] options = {Message._23, Message._24};
        int choice = useMenuScanner(options, "O que queres dizer ao António?");

        switch (choice) {
            case 1:
                goTo23();
                break;
            case 2:
                goTo24();
                break;
            default:
                break;
        }

    }

    public void goTo23() {

        out.println(Message._70);
        goToBeginning();
    }

    public void goTo24() {

        threadSleep(500);
        out.println(Message._24_1);
        threadSleep(500);
        String[] options = {Message._25, Message._26, Message._27};
        int choice = useMenuScanner(options, Message._CHOOSE);

        // TODO adicionar condição de ter CDs

        switch (choice) {
            case 1:
                goTo25();
                break;
            case 2:
                goTo26();
                break;
            case 3:
                goTo27();
                break;
            default:
                break;
        }

    }

    public void goTo25() {

        out.println(Message._28);
        goTo28();

    }

    public void goTo26() {

        out.println(Message._28);
        goTo28();

    }

    public void goTo27() {

        out.println(Message._29);
        goTo29();

    }

    public void goTo28() {

       goToBeginning();

    }

    public void goTo29() {

        //win();

    }

    public void goTo30() {
        out.println(Message._2);
        goTo2();
        //TODO criar condição para não dar mais para falar com o Nosk
    }

    public void goTo31() {

        out.println(Message._32);
        goTo32();

    }

    public void goTo32() {

        String[] options = {Message._33, Message._42};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo33();
                break;
            case 2:
                goTo42();
                break;
            default:
                break;
        }

    }

    public void goTo33() {

        out.println(Message._34);
        goTo34();

    }

    public void goTo34() {

        String[] options = {Message._35, Message._36};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo35();
                break;
            case 2:
                goTo36();
                break;
            default:
                break;
        }

    }

    public void goTo35() {

        out.println(Message._37);
        goTo37();

    }

    public void goTo36() {

        out.println(Message._37);
        goTo37();

    }

    public void goTo37() {

        String[] options = {Message._38, Message._39};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo38();
                break;
            case 2:
                goTo39();
                break;
            default:
                break;
        }

    }

    public void goTo38() {

        //lose();

    }

    public void goTo39() {

        String[] options = {Message._40, Message._41};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo40();
                break;
            case 2:
                goTo41();
                break;
            default:
                break;
        }

    }

    public void goTo40() {

        //lose();

    }

    public void goTo41() {

        //win();

    }

    public void goTo42() {
        CoffeeStory coffeeStory = new CoffeeStory(in,out);
        coffeeStory.goTo43();
    }

    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void threadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
