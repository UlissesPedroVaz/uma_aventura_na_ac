package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class UlissesStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;

    public UlissesStory(InputStream in, PrintStream out) {
        prompt = new Prompt(this.in=in,this.out=out);
    }

    public void goToBeginning() {
        History history = new History(in,out);
        history.start();
    }

    public void goTo3() {
        out.println(Message._3);
        goTo8();
    }

    public void goTo8() {
        out.println(Message._8);
        goTo54();
    }

    public void goTo54() {

        out.println(Message._54);
        String[] options = {Message._55, Message._56};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo55();
                break;
            case 2:
                goTo56();
                break;
            default:
                break;
        }
    }

   public void goTo55() {
       out.println(Message._64);
   }

   public void goTo56() {
       out.println(Message._65);
       goTo57();
   }

   public void goTo57() {
       out.println(Message._57);
   }

    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void useThreadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
