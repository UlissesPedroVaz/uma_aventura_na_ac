import org.academiadecodigo.atticmainkeys.Story.History;
import org.academiadecodigo.atticmainkeys.Story.Message;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;
import sun.tools.jconsole.Worker;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class Server {


    public final static int DEFAULT_PORT = 8000;
    private ServerSocket serverSocket;

    public final String QUIT = "/QUIT";
    public final String MENU = "/MENU";
    public final String CLOSE = "/CLOSE";

    private List<ServerWorker> workers = Collections.synchronizedList(new ArrayList<>());


    public static void main(String[] args) {

        int port = DEFAULT_PORT;

        Server chatServer = new Server();
        chatServer.start(port);

    }

    public void start(int port) {

        int connectionCount = 0;
        Scanner serverIn = new Scanner(System.in);


        try {

            // Bind to local port
            System.out.println("Binding to port " + port + ", please wait  ...");
            serverSocket = new ServerSocket(port);
            System.out.println("Server started: " + serverSocket);



            while (true) {




                // Block waiting for client connections
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client accepted: " + clientSocket);



                try {
                    // Create a new Server Worker
                    connectionCount++;
                    String name = "Client-" + connectionCount;
                    ServerWorker worker = new ServerWorker(name, clientSocket, this);
                    workers.add(worker);

                    // Serve the client connection with a new Thread
                    Thread thread = new Thread(worker);
                    thread.setName(name);
                    thread.start();

                    String close = serverIn.nextLine();
                    if (close.toUpperCase().equals(CLOSE)){
                        quit(clientSocket);
                        close();

                        System.out.println("close server");
                    }


                } catch (IOException | InterruptedException ex) {
                    System.out.println("Error receiving client connection: " + ex.getMessage());
                }

            }

        } catch (IOException e) {
            System.out.println("Unable to start server on port " + port);
        }

    }

    public void quit(Socket clientSocket) {

        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void close (){
        try {
            //quit(clientSocket);
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public class ServerWorker implements Runnable {

        // Immutable state, no need to lock
        final private String name;
        final private Socket clientSocket;
        final private BufferedReader in;
        final private BufferedWriter out;
        public Menu principalMenu;
        private History history;
        private Server server;


        public ServerWorker(String name, Socket clientSocket, Server server) throws IOException, InterruptedException {

            this.name = name;
            this.clientSocket = clientSocket;
            this.server = server;

            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
            history = new History(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()));


        }

        public String getName() {
            return name;
        }


        @Override
        public void run() {
            try {
                principalMenu = new Menu(clientSocket.getInputStream(), new PrintStream(clientSocket.getOutputStream()), history, server);
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Thread " + name + " started");

            try {

                String ins = Message._INTRO;
                byte[] bytes = ins.getBytes(StandardCharsets.UTF_8);
                clientSocket.getOutputStream().write(bytes);

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                principalMenu.startMenu(clientSocket, this);


                while (!clientSocket.isClosed()) {

                    // Blocks waiting for client messages
                    String line = in.readLine();

                    if (line.toUpperCase().equals(QUIT)) {

                        System.out.println("Client " + name + " closed, exiting...");
                        in.close();
                        clientSocket.close();
                        //continue;

                    }else if (line.toUpperCase().equals(MENU)) {
                        System.out.println("Client " + name + " backing to menu...");
                        principalMenu.startMenu(clientSocket, this);

                    }else {
                        System.out.println("idc");
                    }


                }


                workers.remove(this);

            } catch (IOException ex) {
                System.out.println("Receiving error on " + name + " : " + ex.getMessage());
            }

        }

    }

}

