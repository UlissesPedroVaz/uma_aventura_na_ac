package org.academiadecodigo.atticmainkeys.Story;

import java.util.zip.ZipEntry;

public class Message {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_RED = "\u001B[31m";

    public static final String _INTRO = ANSI_CYAN + "\nOh não! A Sara desapareceu! Nunca mais a vamos ver chegar com o seu casaco guna amarelo :(\n" +
            "... ou maybe vamos?\n" +
            "'VEeEMMM COMIGO VIVEeEeeR, UMA AVENTURA'\n" +
            "A tua missão é encontrar a Sara. Ao longo do jogo vais ter de encarnar o Sherlock Holmes que há dentro de ti e descobrir porque é que ela não se dignou a vir trabalhar hoje.\n" + ANSI_RESET;
    public static final String _0 = "\nAcabaste de chegar à Academia e vais começar a tua investigação.\n" +
            "Tens 3 opções: ir à sala dos MC's, ir beber um café ao pátio ou ir falar com o Ulisses que acabou agora de entrar e medir a temperatura.\n";
    public static final String _1 = ANSI_PURPLE + "O que queres fazer?" + ANSI_RESET;
    public static final String _2 = "Ir à sala dos MC's";
    public static final String _3 = "Falar com o Ulisses";
    public static final String _4 = "Ir beber café ao pátio";
    public static final String _4_1 = "\nVais buscar café à máquina e depois andas até ao pátio para apanhar ar fresco nessa cara feia.";
    public static final String _5 = "Perguntar ao Ruivo pela Sara";
    public static final String _6 = "Perguntar ao Rolo pela Sara";
    public static final String _7 = "Falar com o Rolo da Wish (ou Nosk, Nozk, whatever)";
    public static final String _8 = "\nUlisses: 'Olá amiguinho/a! Queres um abracinho?'\nTu: 'Como tu hoje não estás coisinho, quero!'";
    public static final String _9 = "Falar com a Bri";
    public static final String _10 = "Constatar o grande nariz do Zé";
    public static final String _11 = "\nRuivo: 'Hoje ainda não vi a Sara. Mas achas que a devia ter visto? " +
            "Será que o trabalho faz parte da natureza humana? Será que é moralmente errado não querer trabalhar? " +
            "Devemos disciplinar a curiosidade? Qual é a principal causa da infelicidade? " +
            "Será que os nossos pontos de vista sobre a realidade mudam a realidade?'";
    public static final String _11_1 = "Tu: '... Ok...'";
    public static final String _11_2 = "Com o Ruivo não vais dar a lado nenhum. É melhor escolher outra pessoa.";
    public static final String _12 = "\nRolo: 'Epa, eu não a vi. Mas o Bento vive com ela, vai falar com ele.'\n" +
            "Embora o Rolo tenha sido um pouco inútil, não foi por completo.\n" +
            "Tu sobes as escadas para ficares mais perto do céu e vês o Bento a saltitar na bola de pilates.";
    public static final String _12_1 = "\nBento: 'E quie?'";
    public static final String _12_2 = "Tu: 'Viste a Sara?'";
    public static final String _13 = "Bento: 'Não a vejo deste ontem. Ela foi jantar ao Mercado Oriental.'\n\nTu vais ao Mercado Oriental à procura de pistas.";
    public static final String _14 = "Falar com o dono do restaurante";
    public static final String _15 = "'Ahn?'";
    public static final String _16 = "'Cumo?'";
    public static final String _17 = "\nSenhor: '나는 그것에 대해 알고 싶지 않습니다.'";
    public static final String _17_1 = "Isto não vai resultar...\n\n" +
            "Estás a precisar e vais à casa de banho.";
    public static final String _18 = "Ir à casa de banho";
    public static final String _18_1 = "\nO que é aquilo ao pé da sanita?\n" +
            "Aproximas-te e vês... dois CDs? Quem é que ainda tem CDs?\n" +
            "Espera... São CDs dos Tokyo Hotel e dos D'ZRT! Isto certamente tem de ser da Sara!!";
    public static final String _19 = "Voltar à Academia";
    public static final String _20 = "Ir a casa da Sara";
    public static final String _20_1 = "\nAcabaste de chegar a casa da Sara. Realisticamente, só tens duas opções buddy.";
    public static final String _21 = "Tocar à campainha";
    public static final String _21_1 = "\nEsperas um bocado e ouves alguém que parece muito o Nuno falar pelo intercomunicador.";
    public static final String _21_2 = "Nuno??: 'Não me chateiem! Tenho de limpar a casa de banho que a Sara sujou ontem à noite e agora não acorda.'";
    public static final String _21_3 = "Vais mesmo ter de ligar ao António, mas bates palmas ao Nuno à mesma.";
    public static final String _22 = "Ligar ao António";
    public static final String _23 = "'Preciso da chave da casa da Sara'";
    public static final String _24 = "'Preciso de uma aparelhagem'";
    public static final String _24_1 = "\nO António é brutal e traz uma aparelhagem.";
    public static final String _25 = "Tocar Tokyo Hotel";
    public static final String _26 = "Tocar Arctic Monkeys";
    public static final String _27 = "Tocar D'ZRT";
    public static final String _28 = "\nO Sérgio está no seu passeio matinal a cantarolar e, por coincidência, passa por ti enquanto tu estás a tocar a música.";
    public static final String _28_1 = "\nSérgio: 'Mas que linda sinfonia.'";
    public static final String _28_2 = "\nO Sérgio ficou muito impressionado com o teu gosto musical de alto nível!\n" +
            "Ele aconselha-te a desistir da programação porque não tens assim tanto jeito e a envergares por uma carreira musical. Certamente vais ter mais sucesso.";
    public static final String _28_3 = Message.LOSE + "Desistes da Academia e tornas-te um estrela pop com projeção internacional." +
            "\nEsqueces a Sara e todos os teus amigos da Academia." +
            "\nTornas-te convencido e insuportável. Apesar de teres fãs, não tens amigos que realmente se preocupem contigo, só groupies." +
            "\nAcabas sozinho e infeliz.";
    public static final String _28_4 = "\nSérgio: 'Que barulho horrível!'";
    public static final String _28_5 = "\nO Sérgio ficou enojado com o teu gosto musical de merda.\n";
    public static final String _28_6 = "Sérgio: 'Nunca mais te quero ver à frente!'" + Message.LOSE + "Foste expulso porque és uma vergonha e só irias manchar a reputação da Academia. Educa-te musicalmente seu monte de merda!";
    public static final String _29 = "\nVês a porta do prédio a abrir e a Sara sai de casa como uma cobra encantada, enfeitiçada pela nostalgia musical dos D'ZRT.";
    public static final String _29_1 = "\n\n\n===========YOU WIN==========\n\nEncontraste a Sara! Ela ficou com uma intoxicação alimentar e passou a noite agarrada à sanita.\n" +
            "Com o poder majestoso dos D'ZRT conseguiste que ela recuperasse e parasse de ser uma incompetente que não aparece em horário laboral.\n" +
            "És um verdadeiro herói! Palminhas para ti :)";
    public static final String _30 = "Dizer ao Rolo da Wish que ele não sabe dançar.";
    public static final String _30_1 = "\nTu: 'Até o Zé dança melhor que tu.'";
    public static final String _30_2 = "\nRolo da Wish: 'Vai pó caralho! Essas meias nem compridas nem pézinhos tiram-te todo o street cred!'\n" +
            "Ofendeste o ego frágil do Rolo da Wish, ele não quer falar mais contigo. Vai ter com outro MC.";
    public static final String _31 = "Perguntar pela Sara";
    public static final String _31_1 = "\nRolo da Wish: 'Ainda não a vi hoje. A Sopas vive com ela, pode ajudar-te.'";
    public static final String _32 = "\nEntras na sala da cave e vais ter com a Sopas.";
    public static final String _33 = "Perguntar o que ela está a fazer";
    public static final String _33_1 = "\nSopas: 'Lisboa é merda, estou a ver a Casa da Música no Porto'";
    public static final String _33_2 = "Tu: 'LISBOA É FIXE!'\n" +
            "Ficas super ofendido e sais porta fora. Não aguentas continuar a olhar para a cara da Sopas.";
    public static final String _34 = "\nAo sair vais contra o Luís. Ele vê que estás super chateado, então decide ser boa pessoa.";
    public static final String _35 = "Livros de programação";
    public static final String _36 = "CDs tuga dos anos 2000";
    public static final String _37 = "\nO Sérgio passa por vocês.";
    public static final String _38 = "Perguntar algo sobre exceções";
    public static final String _38_1 = "\nO Sérgio fica super feliz por te poder ajudar e começa a dar uma palestra." + Message.LOSE + "O Sérgio nunca mais se irá calar e vai ser impossível a Sara ser encontrada.\n" +
            "Vais ser redirecionado para o início. Vê se fazes melhores escolhas, otário.";
    public static final String _39 = "Pedir o número de telemóvel da Sara";
    public static final String _39_1 = "Sérgio: 'Está no tinder, já devias saber.'\n" +
            "Lidas com a rudeza do Sérgio e vais procurar o número da Sara ao Slack.\n" +
            "Ligas à Sara.";
    public static final String _40 = "\nPor mais que tentes, a Sara não atende." + Message.LOSE + "Claramente não és um dos favoritos da Sara. Ela não quer saber de ti.";
    public static final String _41 = "\nA Sara atende e, como qualquer pessoa normal, tu tocas-lhe o CD dos D'ZRT.";
    public static final String _41_1 = Message.WIN + "Encontraste a Sara! Ela diz-te que ficou com uma intoxicação alimentar e passou a noite agarrada à sanita.\n" +
            "Com o poder majestoso dos D'ZRT conseguiste que ela recuperasse e parasse de ser uma incompetente que não aparece em horário laboral.\n" +
            "És um verdadeiro herói! Palminhas para ti :)";
    public static final String _42 = "Perguntar pela Sara";
    public static final String _42_1 = "Sopas: 'Vi a Sara no pingpong.'\n" +
            "Tu vais até à sala do pingpong.";
    public static final String _43 = "\nEncontras o Bruno e o Casimiro que estão a jogar pingpong.";
    public static final String _43_1 = "\nObservas a sala e..." +
            "\nOH MEU DEUS! Vês a cabeça rapada da Sara de costas para ti!";
    public static final String _44 = "Ir investigar";
    public static final String _45 = "Investigar ir";
    public static final String _46 = "\nAproximaste e tocas-lhe no ombro. Ela vira-se e... WTF?";
    public static final String _46_1 = "\n...???... É o Allan?";
    public static final String _46_2 = "\nO Allan é um grande bacano e, ao ver a tua confusão, decide consolar-te. Ele oferece-te um CD dos Arctic Monkeys e um dos D'ZRT. BRUTAL!";
    public static final String _47 = "\nA Mariana passa e vem ter contigo.";
    public static final String _47_1 = "Bom dia! Vou aos irmãos comprar uma merenda, queres vir?";
    public static final String _48 = "Ir aos irmãos";
    public static final String _49 = "Não ir aos irmãos";
    public static final String _50 = "\nRecebes uma notificação. Os Sofia+Prima+Yoca+Chico enganaram-se no grupo do Slack e escreveram nos atticmainkeys que a Sara gosta dos D'ZRT... Random...\n";
    public static final String _50_1 = "O Luís passa e vê a tua cara de confuso a olhar para o telemóvel. Tem pena de ti e decide ser boa pessoa.";
    public static final String _51 = "\nNo caminho encontras um trilho de sushi. Hmm...";
    public static final String _52 = "Comer um pedaço de sushi do chão";
    public static final String _53 = "Seguir o trilho de sushi";
    public static final String _53_1 = "\nQue sorte! Parece que o trilho de sushi vai levar-te até casa da Sara!";
    public static final String _54 = "Enquanto sentes o agradável quentinho que irradia do corpo do Ulisses, passa o Ruben a gritar AVIÃO";
    public static final String _55 = "Dar 5€ ao Ruben";
    public static final String _56 = "Não dar 5€ ao Ruben, ele que se foda";
    public static final String _57 = "Enquanto estás preplexo com mais uma instância da rudez habitual do Ruben aparece a Luísa!\n" +
            "Luísa: 'Tenho aqui o JAR do Farty Unicorn, é um jogo que faz uma crítica de filmes de ação e de feijoada!'"
            + Message.LOSE + "Passaste o resto do dia colado ao ecrã a jogar Farty Unicorn, não aprendeste nada de programação, " +
            "mas aprendeste que comida picante e metrelhadoras não são uma boa combinação...";
    public static final String _58 = "\nZé: 'Isso é relativo ao tamanho da piça.'";
    public static final String _59 = "Gozar mais com o Zé";
    public static final String _60 = "Perguntar ao Zé se viu a Sara";
    public static final String _61 = "\nGANDA BURN!!\nO Barbas, que estava a passar e ouviu, começa a rir tanto que não consegue parar e corre para a casa de banho. Tu és um bom amigo e vais atrás dele.";
    public static final String _62 = "Seguir o barulho que vem da sala de pingpong";
    public static final String _63 = "Ficar com o Barbas que está a vomitar de tanto rir";
    public static final String _CHOOSE = ANSI_BLUE + "Queres:" + ANSI_RESET;

    public static final String _64 = "\nRuben: 'Obrigado amigo, bora ao Avião que se foda a Sara!'" + Message.WIN + "Ganhaste com Pay2Win, não sabes da Sara, mas foste ao Avião e por isso tudo ficou bem!";
    public static final String _65 = "\nRuben: 'Então vai pó caralho!'";

    public static final String _66 = "\nBri: 'Olha, o Ulisses estava à tua procura.'";
    public static final String _67 = "Zé: 'Gozaste com o meu nariz, por isso GET FUCKED!!' " + Message.LOSE + "O Zé mandou-te um clássico GET FUCKED dos seus. Não aguentaste e foste para casa chorar...";
    public static final String _68 = Message.LOSE + "No que é que estavas a pensar?! Foste juntar-te ao Barbas na casa de banho e passaste lá o resto do dia...";
    public static final String _69 = Message.LOSE + "No que é que estavas a pensar?! Passaste o resto do dia na casa de banho...";
    public static final String _70 = "António: 'Metes-me nojo! Volta para a Academia, tens aulas!'" + Message.LOSE + "INVASOR DE PROPRIEDADE PRIVADA! O António ficou repugnado com a tua falta de civismo...";
    public static final String _71 = "\nSenhor: '난 그녀에게 지쳤어, 그녀는 항상 여기 있어. 그리고 그녀는 그녀가 한국말을 아주 잘한다고 생각하지만 그것은 거짓말입니다.'";

    public static final String LOSE = ANSI_RED + "\n\n\n===========YOU LOSE==========\n\n" + ANSI_RESET;
    public static final String WIN = ANSI_GREEN + "\n\n\n===========YOU WIN==========\n\n" + ANSI_RESET;

}
