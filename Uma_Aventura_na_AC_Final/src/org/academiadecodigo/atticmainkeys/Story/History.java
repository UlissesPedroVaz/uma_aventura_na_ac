package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class History {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;
    private boolean hasDzrtCD = false;
    private boolean hasAMCD = false;
    private boolean hasTHCD = false;


    public History (InputStream in, PrintStream out) {
        prompt = new Prompt(this.in=in,this.out=out);
    }

    public void start(){


        // options that you want to be presented are supplied in an array of strings
        String[] options = {Message._2, Message._4, Message._3};

        out.println(Message._0);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // create a menu with those options and set the message
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(Message._1);

        int choice = prompt.getUserInput(scanner);

        switch (choice) {
            case 1:
                MCRoomStory mcRoomStory = new MCRoomStory(in,out, this);
                mcRoomStory.goTo2();
                break;
            case 2:
                CoffeeStory coffeeStory = new CoffeeStory(in,out, this);
                coffeeStory.goTo4();
                break;
            case 3:
                UlissesStory ulissesStory = new UlissesStory(in,out, this);
                ulissesStory.goTo3();
            default:
                break;
        }
    }

    public void setHasAMCD(boolean hasAMCD) {
        this.hasAMCD = hasAMCD;
    }

    public boolean isHasAMCD() {
        return hasAMCD;
    }

    public void setHasDzrtCD(boolean hasDzrtCD) {
        this.hasDzrtCD = hasDzrtCD;
    }

    public boolean isHasDzrtCD() {
        return hasDzrtCD;
    }

    public void setHasTHCD(boolean hasTHCD) {
        this.hasTHCD = hasTHCD;
    }

    public boolean isHasTHCD() {
        return hasTHCD;
    }

}

