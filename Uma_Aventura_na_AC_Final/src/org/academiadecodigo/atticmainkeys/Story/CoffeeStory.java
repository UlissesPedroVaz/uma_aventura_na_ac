package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class CoffeeStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;
    public static boolean barbas = false;
    private final History history;

    public CoffeeStory(InputStream in, PrintStream out, History history) {
        prompt = new Prompt(this.in=in,this.out=out);
        this.history = history;
    }

    public void goTo4() {

        threadSleep(500);
        out.println(Message._4_1);
        threadSleep(500);

        String[] options = {Message._9, Message._10};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo66();
                break;
            case 2:
                goTo58();
                break;
            default:
                break;
        }
    }

    public void goTo66() {
        threadSleep(500);
        out.println(Message._66);
        threadSleep(500);
        UlissesStory ulissesStory = new UlissesStory(in,out , history);
        ulissesStory.goTo3();
    }

    public void goTo58() {

        out.println(Message._58);
        String[] options = {Message._59, Message._60};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo61();
                break;
            case 2:
                System.out.println(Message._67);
                break;
            default:
                break;
        }
    }

    public void goTo61() {

        threadSleep(500);
        out.println(Message._61);
        String[] options = {Message._62, Message._63};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo43();
                break;
            case 2:
                goTo47();
                break;
            default:
                break;
        }
        barbas = true;
    }

    public void goTo43() {
        threadSleep(500);
        out.println(Message._43);
        threadSleep(500);
        out.println(Message._43_1);
        threadSleep(500);

        String[] options = {Message._44, Message._45};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
            case 2:
                goTo46();
                break;
            default:
                break;
        }
    }

    public void goTo46() {
        threadSleep(500);
        out.println(Message._46);
        threadSleep(500);
        out.println(Message._46_1);
        threadSleep(500);
        out.println(Message._46_2);
        history.setHasAMCD(true);
        history.setHasDzrtCD(true);
        goTo47();
    }

    public void goTo47() {
        threadSleep(500);
        out.println(Message._47);
        threadSleep(500);
        out.println(Message._47_1);
        threadSleep(500);
        String[] options = {Message._48, Message._49};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo51();
                break;
            case 2:
                goTo50();
                break;
            default:
                break;
        }
    }

    public void goTo50() {
        threadSleep(500);
        out.println(Message._50);
        threadSleep(500);
        out.println(Message._50_1);
        threadSleep(500);
        MCRoomStory mcRoomStory = new MCRoomStory(in,out,history);
        mcRoomStory.goTo34();

    }

    public void goTo51() {
        threadSleep(500);
        out.println(Message._51);
        threadSleep(500);
        String[] options = {Message._52, Message._53};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo68or69();
                break;
            case 2:
                goTo53();
                break;
            default:
                break;
        }
    }

    public void goTo68or69() {
        if (barbas) {
            out.println(Message._68);
        } else {
            out.println(Message._69);
        }
    }

    public void goTo53() {

        threadSleep(500);
        MCRoomStory mcRoomStory = new MCRoomStory(in,out, history);
        out.println(Message._53_1);
        mcRoomStory.goTo20();
    }



    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void threadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
