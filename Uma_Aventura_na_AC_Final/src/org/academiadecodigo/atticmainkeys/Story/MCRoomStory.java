package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class MCRoomStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;
    private boolean talkedToNozk = false;
    private boolean talkedToRuivo = false;
    private final History history;

    public MCRoomStory(InputStream in, PrintStream out, History history) {
        prompt = new Prompt(this.in=in,this.out=out);
        this.history = history;
    }

    public void goToBeginning() {
        History history = new History(in,out);
        history.start();
    }

    public void goTo2() {

        threadSleep(700);

        if (!talkedToNozk && !talkedToRuivo) {
            out.println("\nEstás à porta da sala dos MC's. Vês o Ruivo, o Rolo e o Rolo da Wish.");

            String[] options = {Message._5, Message._6, Message._7};
            int choice = useMenuScanner(options, Message._CHOOSE);

            threadSleep(500);

            switch (choice) {
                case 1:
                    goTo5();
                    break;
                case 2:
                    goTo6();
                    break;
                case 3:
                    goTo7();
                    break;
                default:
                    break;
            }
        } else if (talkedToNozk && !talkedToRuivo) {
            out.println("\nO Rolo da wish amuou e foi embora.");

            String[] options = {Message._5, Message._6};
            int choice = useMenuScanner(options, Message._CHOOSE);

            threadSleep(500);

            switch (choice) {
                case 1:
                    goTo5();
                    break;
                case 2:
                    goTo6();
                    break;
                default:
                    break;
            }
        } else if (!talkedToNozk && talkedToRuivo) {
            out.println("\nTens de falar com mais alguém que não tenha fritado a cabeça em Paris.");

            String[] options = {Message._6, Message._7};
            int choice = useMenuScanner(options, Message._CHOOSE);

            threadSleep(500);

            switch (choice) {
                case 1:
                    goTo6();
                    break;
                case 2:
                    goTo7();
                    break;
                default:
                    break;
            }
        } else {
            out.println("\nEu sei que não querias nada falar com o Rolo, mas tens mesmo de ir. Desculpa.");

            String[] options = {Message._6};
            int choice = useMenuScanner(options, Message._CHOOSE);

            threadSleep(500);

            if (choice == 1) {
                goTo6();
            }
        }
    }

    public void goTo5() {

        threadSleep(500);
        out.println(Message._11);
        threadSleep(500);
        out.println(Message._11_1);
        threadSleep(500);
        out.println(Message._11_2);
        talkedToRuivo = true;
        goTo2();

    }

    public void goTo6() {
        out.println(Message._12);
        threadSleep(500);
        out.println(Message._12_1);
        threadSleep(500);
        out.println(Message._12_2);
        threadSleep(500);
        out.println(Message._13);

        threadSleep(500);

        String[] options = {Message._14, Message._18};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo14();
                break;
            case 2:
                goTo18();
                break;
            default:
                break;
        }

    }

    public void goTo7() {

        String[] options = {Message._30, Message._31};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo30();
                break;
            case 2:
                goTo31();
                break;
            default:
                break;
        }

    }

    public void goTo14() {

        threadSleep(500);
        out.println(Message._71);
        threadSleep(500);

        String[] options = {Message._15, Message._16};
        int choice = useMenuScanner(options, Message.ANSI_BLUE + "WTF?" + Message.ANSI_RESET);

        switch (choice) {
            case 1:
                goTo15();
                break;
            case 2:
                goTo16();
                break;
            default:
                break;
        }

    }

    public void goTo15() {

        threadSleep(500);
        out.println(Message._17);
        goTo17();

    }

    public void goTo16() {

        out.println(Message._17);
        goTo17();

    }

    public void goTo17() {

        threadSleep(500);
        out.println(Message._17_1);
        threadSleep(500);
        goTo18();

    }

    public void goTo18() {

        threadSleep(500);
        out.println(Message._18_1);
        threadSleep(700);

        history.setHasDzrtCD(true);
        history.setHasTHCD(true);

        String[] options = {Message._19, Message._20};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo19();
                break;
            case 2:
                goTo20();
                break;
            default:
                break;
        }

    }

    public void goTo19() {
        out.println(Message._INTRO);
        goToBeginning();
    }

    public void goTo20() {

        threadSleep(500);
        out.println(Message._20_1);
        threadSleep(500);

        String[] options = {Message._21, Message._22};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo21();
                break;
            case 2:
                goTo22();
                break;
            default:
                break;
        }

    }

    public void goTo21() {

        threadSleep(500);
        out.println(Message._21_1);
        threadSleep(500);
        out.println(Message._21_2);
        threadSleep(500);

        out.println(Message._21_3);
        goTo22();

    }

    public void goTo22() {

        threadSleep(500);

        String[] options = {Message._23, Message._24};
        int choice = useMenuScanner(options, Message.ANSI_BLUE + "O que queres dizer ao António?" + Message.ANSI_RESET);

        switch (choice) {
            case 1:
                goTo23();
                break;
            case 2:
                goTo24();
                break;
            default:
                break;
        }

    }

    public void goTo23() {

        out.println(Message._70);

    }

    public void goTo24() {

        out.println(history.isHasDzrtCD() + " " + history.isHasAMCD() + " " + history.isHasTHCD());

        threadSleep(500);
        out.println(Message._24_1);
        threadSleep(500);

        if (history.isHasAMCD() && history.isHasDzrtCD()) {
            String[] options = {Message._26, Message._27};
            int choice = useMenuScanner(options, Message._CHOOSE);

            switch (choice) {
                case 1:
                    goTo26();
                    break;
                case 2:
                    goTo27();
                    break;
                default:
                    break;
            }
        } else if (history.isHasTHCD() && history.isHasDzrtCD()) {
            String[] options = {Message._25, Message._27};
            int choice = useMenuScanner(options, Message._CHOOSE);

            switch (choice) {
                case 1:
                    goTo25();
                    break;
                case 2:
                    goTo27();
                    break;
                default:
                    break;
            }
        } else if (history.isHasDzrtCD() && !history.isHasTHCD() && !history.isHasAMCD()) {
            String[] options = {Message._27};
            int choice = useMenuScanner(options, Message._CHOOSE);

            if (choice == 1) {
                goTo27();
            }
        }
    }

    public void goTo25() {

        threadSleep(500);
        out.println(Message._28);
        goTo28();

    }

    public void goTo26() {

        threadSleep(500);
        out.println(Message._28);
        threadSleep(500);
        out.println(Message._28_4);
        threadSleep(500);
        out.println(Message._28_5);
        threadSleep(500);
        out.println(Message._28_6);

    }

    public void goTo27() {

        threadSleep(500);
        out.println(Message._29);
        goTo29();

    }

    public void goTo28() {

        threadSleep(500);
        out.println(Message._28_1);
        threadSleep(500);
        out.println(Message._28_2);
        threadSleep(500);
        out.println(Message._28_3);

    }

    public void goTo29() {

        threadSleep(500);
        out.println(Message._29_1);

    }

    public void goTo30() {

        threadSleep(500);
        out.println(Message._30_1);
        threadSleep(500);
        out.println(Message._30_2);
        threadSleep(500);
        talkedToNozk = true;
        goTo2();

    }

    public void goTo31() {

        threadSleep(500);
        out.println(Message._31_1);
        threadSleep(500);
        out.println(Message._32);
        goTo32();

    }

    public void goTo32() {

        threadSleep(500);

        String[] options = {Message._33, Message._42};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo33();
                break;
            case 2:
                goTo42();
                break;
            default:
                break;
        }

    }

    public void goTo33() {

        threadSleep(500);
        out.println(Message._33_1);
        threadSleep(500);
        out.println(Message._33_2);
        threadSleep(500);
        out.println(Message._34);
        goTo34();

    }

    public void goTo34() {

        String[] options = {Message._35, Message._36};
        int choice = useMenuScanner(options, Message.ANSI_BLUE + "Dá-te a escolher entre:" + Message.ANSI_RESET);

        switch (choice) {
            case 1:
                goTo35();
                break;
            case 2:
                goTo36();
                break;
            default:
                break;
        }

    }

    public void goTo35() {

        out.println(Message._37);
        goTo37();

    }

    public void goTo36() {

        out.println(Message._37);
        history.setHasDzrtCD(true);
        goTo37();

    }

    public void goTo37() {

        String[] options = {Message._38, Message._39};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo38();
                break;
            case 2:
                goTo39();
                break;
            default:
                break;
        }

    }

    public void goTo38() {

        threadSleep(500);
        out.println(Message._38_1);
        threadSleep(500);
        goToBeginning();

    }

    public void goTo39() {

        threadSleep(500);
        out.println(Message._39_1);

        if (!history.isHasDzrtCD()) {
            goTo40();
            out.println("\n\n\nWas here\n\n\n");
        }
        else if (history.isHasDzrtCD()){
            goTo41();
        }

    }

    public void goTo40() {

        threadSleep(500);
        out.println(Message._40);
        goToBeginning();

    }

    public void goTo41() {

        threadSleep(500);
        out.println(Message._41);
        threadSleep(500);
        out.println(Message._41_1);
        threadSleep(500);
        goToBeginning();

    }

    public void goTo42() {

        threadSleep(500);
        out.println(Message._42_1);
        CoffeeStory coffeeStory = new CoffeeStory(in,out, history);
        coffeeStory.goTo43();
    }

    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void threadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
