package org.academiadecodigo.atticmainkeys.Story;

import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;

public class UlissesStory {

    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;
    private final History history;

    public UlissesStory(InputStream in, PrintStream out, History history) {
        prompt = new Prompt(this.in=in,this.out=out);
        this.history = history;
    }

    public void goToBeginning() {
        History history = new History(in,out);
        history.start();
    }

    public void goTo3() {
        threadSleep(500);
        goTo8();
    }

    public void goTo8() {
        threadSleep(500);
        out.println(Message._8);
        goTo54();
    }

    public void goTo54() {

        threadSleep(500);
        out.println(Message._54);
        String[] options = {Message._55, Message._56};
        int choice = useMenuScanner(options, Message._CHOOSE);

        switch (choice) {
            case 1:
                goTo55();
                break;
            case 2:
                goTo56();
                break;
            default:
                break;
        }
    }

   public void goTo55() {
        threadSleep(500);
        out.println(Message._64);
        threadSleep(500);

   }

   public void goTo56() {
        threadSleep(500);
       out.println(Message._65);
       goTo57();
   }

   public void goTo57() {
        threadSleep(500);
       out.println(Message._57);
       threadSleep(500);
       goToBeginning();
   }

    private int useMenuScanner(String[] options, String message) {
        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage(message);
        return prompt.getUserInput(scanner);
    }

    private void threadSleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
