import org.academiadecodigo.atticmainkeys.Story.History;
import org.academiadecodigo.atticmainkeys.Story.Message;
import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.menu.MenuInputScanner;

import java.io.InputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Menu {
    private final Prompt prompt;
    private final InputStream in;
    private final PrintStream out;

    private History history;
    private Server server;


    public Menu(InputStream in, PrintStream out, History history, Server server) {
        prompt = new Prompt(in, out);
        this.in = in;
        this.out = out;
        this.history = history;
        this.server = server;
    }


    public void startMenu(Socket clientSocket, Server.ServerWorker serverWorker) {

        String[] options = {"Jogar", "Instruções", "Sair"};

        MenuInputScanner scanner = new MenuInputScanner(options);
        scanner.setMessage("Escolhe uma opção:");

        int answerIndex = prompt.getUserInput(scanner);

        if (answerIndex == 1) {
            System.out.println("jogar");
            history.start();
            //bole = false;
        }
        if (answerIndex == 2) {
            String s = "\n GET FUCKED!!! \n \n" + "'/quit' para sair\n" + "'/menu' para voltares ao menu\n";
            out.println(s);
        }
        if (answerIndex == 3) {
            server.quit(clientSocket);

        }


    }


    public Prompt getPrompt() {
        return prompt;
    }


}